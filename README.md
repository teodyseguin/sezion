### SEZION TEST PROJECT

### TECH USED 

- ReactJS
- Babel
- Webpack
- NPM

### PRE-REQUISITE

- You must have Node and NPM installed on your local machine.

### HOW TO RUN

- Clone this repository. Once complete change directory to the root folder.
- Run `npm install` to download it's dependencies
- After the dependencies are downloaded, run `npm run dev`
- Open your browser, and navigate to [http://localhost:8080/www](http://localhost:8080/www)

