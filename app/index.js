import React from 'react';
import ReactDOM from 'react-dom';
import ReportGenerator from './shared/ReportGenerator';
import '../www/assets/css/bootstrap.min';

/**
 * Inject specific component per page
 */
const pages = {
  'home-page': {
    component: <ReportGenerator />,
    container: 'json-report-generator-com'
  }
};

Object.keys(pages).forEach((pageClass) => {
  if (document.getElementsByClassName(pageClass).length) {
    ReactDOM.render(
      pages[pageClass].component,
      document.getElementById(pages[pageClass].container)
    );

    return;
  }
});

