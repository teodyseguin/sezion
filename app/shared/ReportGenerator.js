import React from 'react';
import ReactDOM from 'react-dom';
import _ from 'lodash';

/**
 * Creates the initial header for the
 * table report
 */
function TableReportHeader(props) {
  let headers = [];

  Object.keys(props.parsedData).forEach((value, currentIndex) => {
    if (value !== 'composition' && value !== 'duration') {
      headers.push(<th key={currentIndex}>{value}</th>); 
    }
  });

  return <tr>{headers}</tr>;
}

/**
 * Very basic validation. Can be extended later
 *
 * @param data
 *   data that comes from the input field
 *
 * @return JSON || false
 */
function isValidComposition(data) {
  let json = null;
  
  try {
    json = JSON.parse(data);
  }
  catch(e) {
    return false;
  }

  if (!_.isArray(json) || !json.length) {
    return false;
  }

  return json;
}

/**
 * First level of validating the data
 * being inputted by the user
 *
 * @param inputField
 *   a reference to the input field DOM Object
 *
 * @return composition object || false
 */
function generateFromInput(inputField) {
  if (inputField.value === '') {
    return false;
  }

  let composition = isValidComposition(inputField.value);

  if (!composition) {
    inputField.value = '';
    return false;
  }

  inputField.value = '';

  return composition;
}

/**
 * A component to create the inner
 * details for videos
 *
 * @param props.videos
 *   An array of video objects
 *
 * @return <table>
 */
function MediaDetails(props) {
  let headers = [];
  let data = [];
  let obj = {};

  // we create a dynamic property for obj
  // based on the given format string value
  // and initialize it as an empty array
  props.media.forEach(key => {
    if (key.format) {
      obj[key.format] = []; 
    }
    else {
      obj['noFormat'] = []; 
    }
  });

  // after initializing the property, we
  // iterate again to push the objects
  props.media.forEach(key => {
    if (key.format) {
      obj[key.format].push(key); 
    }
    else {
      obj.noFormat.push(key);
    }
  });

  Object.keys(obj).forEach((key, index) => {
    headers.push(<th key={index}>{key}</th>);
    data.push(<td key={index}>{obj[key].length}</td>)
  });

  return (
    <table className="table">
      <thead><tr>{headers}</tr></thead>
      <tbody>
        <tr>{data}</tr>
      </tbody>
    </table>
  );
}

export default class ReportGenerator extends React.Component {
  constructor(props) {
    super(props);

    this.parsedData = {
      video: [],
      audio: [],
      text:  [],
      image: [],
      composition: [],
      duration: 0
    };

    this.state = {
      video: [],
      audio: [],
      text: [],
      image: [],
      composition: [],
      duration: 0
    };
  }

  handleClick(e) {
    let self = this;

    switch (e.target.id) {
      case 'from-mock':
        $.ajax({
          dataType: 'json',
          url: 'assets/mock/composition-mock.json',
          success(data) {
            self.parseData(data);
            self.updateState(self);
          }
        });

        break;

      case 'from-input':
        let result = generateFromInput(document.getElementById('composition-input'));

        if (!result) {
          alert('Please enter a valid composition data');
          return;
        }

        self.parseData(result);
        self.updateState(self);

        break;

      case 'reset-report':
        self.resetState(self);

        break;
    }
  }

  parseData(data) {
    let self = this;

    if (!_.isArray(data)) {
      return;
    }

    Object.keys(data).forEach((value) => {
      if (data[value].hasOwnProperty('type')) {
        if (data[value].type === 'composition') {
          self.parseData(data[value].composition);
        }
        else {
          self.parsedData[data[value].type].push(data[value]);
          self.parsedData.duration += parseInt(data[value].duration);
        }
      }
    });
  }

  resetParsedData(self) {
    self.parsedData = {
      video: [],
      audio: [],
      text:  [],
      image: [],
      composition: [],
      duration: 0
    };
  }

  resetState(self) {
    self.setState({
      video: [],
      audio: [],
      text: [],
      image: [],
      composition: [],
      duration: 0
    });
  }

  render() {
    return (
      <div>
      <form>
        <div className="form-group mb-2">
          <input
            type="text"
            id="composition-input"
            className="form-control"
            placeholder="Enter composition json code here"
            defaultValue=""
          />
        </div>

        <div className="buttons">
          <button
            type="button"
            id="from-input"
            className="btn btn-primary mr-1"
            onClick={(e) => this.handleClick(e)}>
            Generate from Input
          </button>
          <button
            type="button"
            id="from-mock"
            className="btn btn-primary mr-1"
            onClick={(e) => this.handleClick(e)}>
            Generate from composition-mock.json
          </button>
          <button
            type="button"
            id="reset-report"
            className="btn btn-primary" onClick={(e) => this.handleClick(e)}>
            Clear report
          </button>
        </div>
      </form>

      <table className="table mt-2">
        <thead>
          <TableReportHeader parsedData={this.state} />
        </thead>
        <tbody>
          <tr>
            <td>
              {this.state.video.length}
              <MediaDetails media={this.state.video} />
            </td>
            <td>
              {this.state.audio.length}
              <MediaDetails media={this.state.audio} />
            </td>
            <td>
              {this.state.text.length}
              <MediaDetails media={this.state.text} />
            </td>
            <td>
              {this.state.image.length}
              <MediaDetails media={this.state.image} />
            </td>
          </tr>
        </tbody>
      </table>
      <p><strong>Duration: {this.state.duration / 1000} minute(s) {this.state.duration % 1000} second(s)</strong></p>
      </div>
    );
  }

  updateState(self) {
    self.setState({
      video: self.parsedData.video,
      audio: self.parsedData.audio,
      text: self.parsedData.text,
      image: self.parsedData.image,
      duration: self.parsedData.duration
    });

    self.resetParsedData(self);
  }
}

